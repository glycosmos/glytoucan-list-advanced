import {html, css, LitElement} from 'lit'
import {unsafeHTML} from 'lit-html/directives/unsafe-html.js';

export class GlytoucanListAdvanced extends LitElement {

  static get properties() {
    return {
      count:{type: Number},
      text: {type: String},
      limit: {type: String},
      offset: {type: String},
      w:{type: Boolean},
      ct:{type: Boolean},
      all_date :{type: Array},
      all_seq :{type: Array},
      select_format:{type:String}
    }
  }

  constructor() {
    super();
    this.select_format = "";
    this.count=0;
    this.all_date = [];
    this.w = false;
    this.ct = false;
    this.all_seq = [];
    this.text = '<center><div class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div></center>';
    this.limit = 50;
    this.offset = 0;
  }
  connectedCallback() {
    super.connectedCallback();
    this.get_all_data();
  }

  render() {
    return html `
    <style> @import "./index.css"; </style>
    <h1 class="title">Glycan List</h1>
    <p>Loaded ${this.count} glycans. </p>
    ${ this.all_seq.length === 0 || this.all_date.length <= 10000? html`<p>Please be patient for loading all the structures. </p>
    <center><div class="spinner-border" role="status"><span class="visually-hidden">Loading...</span></div></center>` : html`
    <button type="button" class="btn btn-outline-primary" @click=${this.handleDownload}>Download ${this.count} glycans.</button>
    <div class="form-check">
    <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" @click=${this._wurcs}>
    <label class="form-check-label" for="flexRadioDefault1">WURCS</label>
    </div>
    <div class="form-check">
    <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" @click=${this._ct}>
    <label class="form-check-label" for="flexRadioDefault1">GlycoCT</label>
    </div>` }

    <nav aria-label="Page navigation example" style=”margin-left:100%”>
    <ul class="pagination justify-content-end">
    <li class="page-item "><a class="page-link" @click=${this._back}>Previous</a></li>
    <li class="page-item disabled">
    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">${this.offset + 1}-${this.offset + this.limit + 1}</a>
    </li>
    <li class="page-item " ><a class="page-link" @click=${this._next}>Next</a></li>
    </ul>
    </nav>
    </div>
    </div>
    </div>
    ${unsafeHTML(this.reload_list())}`
  }
  _next() {
    this.offset = this.offset + this.limit
    this.reload_list();
  }
  _back() {
    this.offset = this.offset - this.limit
    this.reload_list();
  }
  _wurcs() {
    this.w = true;
    this.ct = false;
    this.reload_list();
  }
  _ct() {
    this.w = false;
    this.ct = true;
    this.reload_list();
  }
  reload_list(){
    var table = "";
    this.list = this.all_date.slice(this.offset , this.offset + this.limit);
    this.list.forEach((value ) => {
      var acc = value[0];
      var date1 = new Date(value[1].last).toLocaleDateString();
      var date2 = new Date(value[1].oldest).toLocaleDateString();
 
      table = `${table}

      <div class="card">
      <div class="card-body">
        <h5 class="card-title">
        <a href="https://glytoucan.org/Structures/Glycans/${acc}">${acc}</a>
        </h5>
        <h6 class="card-subtitle mb-2 text-muted">Last registration Date ${date1.replace(/\//g, '-')}</h6>
        <h6 class="card-subtitle mb-2 text-muted">Oldest registration Date ${date2.replace(/\//g, '-')}</h6>
        <img src="https://image.glycosmos.org/snfg/png/${acc}"
              onerror='this.src = "https://api.glycosmos.org/wurcs2image/latest/png/binary/${acc}";'
              alt="No image has been generated yet">`;
      if (this.ct){
        var ct = this.all_seq[value[0]]["GlycoCT"];
  
        table = `${table}  <div class="card-footer">
                <pre><code>${ct}</code><pre>
                </div>`;
      }
      if(this.w){
        var w = this.all_seq[value[0]]["WURCS"];

        table = `${table} 
              <div class="card-footer">
  <p>${w}</p>
  </div>`;
      }
      table = `${table}
  </div>
  </div>`;

      table = `${table}  </tbody></table></div>`;

    });
    this.text = "";
    return table;
  }

  get_all_data(){
    this.get_seq();
    this.get_date();
  }

  async get_seq() {
    const url = "https://ts.glytoucan.org/servlet/query?query=PREFIX%20rdf%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%3E%0D%0APREFIX%20rdfs%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0D%0APREFIX%20dcterms%3A%20%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0D%0APREFIX%20glycan%3A%20%3Chttp%3A%2F%2Fpurl.jp%2Fbio%2F12%2Fglyco%2Fglycan%23%3E%0D%0APREFIX%20glytoucan%3A%20%20%3Chttp%3A%2F%2Fwww.glytoucan.org%2Fglyco%2Fowl%2Fglytoucan%23%3E%0D%0A%0D%0ASELECT%20DISTINCT%20%3FResEntry%20%3FWurcsSeq%20%3FCtSeq%0D%0A%20%20WHERE%20{%0D%0A%20%20%20GRAPH%20%3Chttp%3A%2F%2Frdf.glytoucan.org%2Fcore%3E%20{%0D%0A%20%20%20%20%20%20%20%20%3FSaccharide%20glycan%3Ahas_resource_entry%20%3FResEntry%20.%0D%0A%20%20%20%20%20%20}%0D%0A%20%20%20%20%20%20%20%3FSaccharide%20glycan%3Ahas_glycosequence%20%3FGSeq%20.%0D%0A%20%20%20%20%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20GRAPH%20%3Chttp%3A%2F%2Frdf.glytoucan.org%2Fsequence%2Fwurcs%3E%20{%0D%0A%20%20%20%20%20%20%20%20optional%20{%0D%0A%20%20%20%20%20%20%20%20%20%20%3FGSeq%20glycan%3Ahas_sequence%20%3FWurcsSeq.%0D%0A%20%20%20%20%20%20}%0D%0A%20%20%20%20%20%20%20%20}%0D%0A%20%20%20%20%20%20GRAPH%20%3Chttp%3A%2F%2Frdf.glytoucan.org%2Fsequence%2Fglycoct%3E%20{%0D%0A%20%20%20%20%20%20%20%20optional%20{%0D%0A%20%20%20%20%20%20%20%20%20%20%3FSaccharide%20glycan%3Ahas_glycosequence%20%3FGSeqct%20.%0D%0A%20%20%20%20%20%20%20%20%20%20%3FGSeqct%20glycan%3Ahas_sequence%20%3FCtSeq%20.%0D%0A%20%20%20%20%20%20%20%20}%0D%0A%20%20%20%20%20%20}%0D%0A%20%20%20%20%20%20FILTER%20NOT%20EXISTS%20{%0D%0A%20%20%20%20%20%20%20GRAPH%20%3Chttp%3A%2F%2Frdf.glytoucan.org%2Farchive%3E%20{%0D%0A%20%20%20%20%20%20%20%20%20%3FSaccharide%20rdf%3Atype%20%3FArchiveSaccharide%0D%0A%20%20%20%20%20%20%20}%0D%0A%20%20}%0D%0A}%0D%0A&format=JSON";
    console.log(url);
    await fetch(url, {
      mode: 'cors'
    }).then(res => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error();
      }
    }).then(response => {
      var items = response.results.bindings;
      var ret = {};
      items.forEach(item => {
      var acc = item.ResEntry.value.split("/").reverse()[0];
      if(!ret[acc]){
        ret[acc] = {WURCS:"",GlycoCT:""};
      }
      if(item.WurcsSeq){
       ret[acc]["WURCS"] =  item.WurcsSeq.value;
      }
      if(item.CtSeq){
      ret[acc]["GlycoCT"] = item.CtSeq.value;
      }
      });
      this.all_seq = ret;
      this.count = Object.keys(ret).length;
      this.select_format = `      <div class="form-check">
      <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" @click=${this._wurcs}>
        <label class="form-check-label" for="flexRadioDefault1">
          WURCS
        </label>
      </div>
          <div class="form-check">
      <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" @click=${this._ct}>
        <label class="form-check-label" for="flexRadioDefault1">
          GlycoCT
        </label>
      </div>`;
    
  //    console.log(Object.keys(ret)[0]);
    })
  //  await this.requestUpdate();
  }

  async get_date() {
    const url = "https://ts.glytoucan.org/servlet/query?query=PREFIX%20rdf%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%3E%0D%0A%20%20%20%20PREFIX%20rdfs%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0D%0A%20%20%20%20PREFIX%20dcterms%3A%20%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0D%0A%20%20%20%20PREFIX%20glycan%3A%20%3Chttp%3A%2F%2Fpurl.jp%2Fbio%2F12%2Fglyco%2Fglycan%23%3E%0D%0A%20%20%20%20PREFIX%20glytoucan%3A%20%20%3Chttp%3A%2F%2Fwww.glytoucan.org%2Fglyco%2Fowl%2Fglytoucan%23%3E%0D%0A%0D%0A%20%20%20%20SELECT%20%3FSaccharide%20%3FDate%0D%0A%20%20%20%20WHERE%20%7B%0D%0A%20%20%20%20%20%20GRAPH%20%3Chttp%3A%2F%2Frdf.glytoucan.org%2Fcore%3E%20%7B%0D%0A%20%20%20%20%20%20%20%20%3FS%20glytoucan%3Ahas_primary_id%20%3FSaccharide.%0D%0A%20%20%20%20%20%20%20%20%3FS%20glycan%3Ahas_resource_entry%20%3FResEntry%20.%0D%0A%20%20%20%20%20%20%20%20%3FResEntry%20glytoucan%3Adate_registered%20%3FDate%20.%0D%0A%20%20%20%20%20%20%7D%0D%0A%20%20%20%20%20%20FILTER%20NOT%20EXISTS%20%7B%0D%0A%20%20%20%20%20%20%20GRAPH%20%3Chttp%3A%2F%2Frdf.glytoucan.org%2Farchive%3E%20%7B%0D%0A%20%20%20%20%20%20%20%20%20%3FS%20rdf%3Atype%20%3FArchiveSaccharide%0D%0A%20%20%20%20%20%20%20%7D%0D%0A%20%20%20%20%20%7D%0D%0A%20%20%20%20%7D&format=JSON";
    console.log(url);
    await fetch(url, {
      mode: 'cors'
    }).then(res => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error();
      }
    }).then(response => {
      var items = response.results.bindings;
      var ret = {};

      items.forEach(item => {
      var acc = item.Saccharide.value.split("/").reverse()[0];
      if(!ret[acc]){
        ret[acc] = {last:item.Date.value,oldest:item.Date.value,history:[item.Date.value]};
      }else{
       ret[acc].history.push(item.Date.value);
      }
      if(ret[acc].last < item.Date.value){
        ret[acc].last = item.Date.value;
      }
      if(ret[acc].oldest > item.Date.value){
        ret[acc].oldest = item.Date.value;
      }
      });
    
      ret = Object.entries(ret);
    
      ret.sort(function(a,b){
        if(a[1].last < b[1].last) return 1;
        if(a[1].last > b[1].last) return -1;
        return 0;
      });
    
      this.all_date = ret;
   //   console.log(ret[0][0]);
    })
  //  await this.requestUpdate();
  }

  handleDownload() {
    var Content = "";
    for (const ele of this.all_date) {
      if (ele[0].length === 8){
if (this.all_seq[ele[0]]){//アーカイブ済みのアクセッションナンバーが混ざることがある。sparqlのせいだが、余裕がないので課題化しておく。
      Content = Content + "\"" + ele[0] + "\",\"" + this.all_seq[ele[0]]["WURCS"] + "\",\"" + this.all_seq[ele[0]]["GlycoCT"].replace( /\r?\n/g , '\n') +"\"\n";
    }
  }
  }
  var blob = new Blob([ Content ], { "type" : "text/plain" });
  const link = document.createElement('a');
  link.download = 'glycan.csv';
  link.href = URL.createObjectURL(blob);
  link.click();
  URL.revokeObjectURL(link.href);
  }
}

window.customElements.define('glytoucan-list-advanced', GlytoucanListAdvanced)
