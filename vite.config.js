import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    lib: {
      entry: 'src/glytoucan-list-advanced.js',
      formats: ['es']
    },
    rollupOptions: {
     // external: /^lit/
    }
  }
})
